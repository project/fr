msgid ""
msgstr ""
"Project-Id-Version: Drupal 6\n"
"POT-Creation-Date: 2007-12-28 11:29+0100\n"
"PO-Revision-Date: 2007-12-30 17:55+0100\n"
"Last-Translator: Damien Tournoud <damz@prealable.org>\n"
"Language-Team: drupalfr.org <traduction@drupalfr.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Poedit-Language: French\n"
"X-Poedit-Country: France\n"
"X-Poedit-SourceCharset: utf-8\n"

#: modules/dblog/dblog.admin.inc:18
msgid "Discard log entries above the following row limit"
msgstr ""

#: modules/dblog/dblog.admin.inc:21
msgid ""
"The maximum number of rows to keep in the database log. Older entries will "
"be automatically discarded. (Requires a correctly configured <a href=\"@cron"
"\">cron maintenance task</a>.)"
msgstr ""

#: modules/dblog/dblog.admin.inc:81;109
msgid "No log messages available."
msgstr "Aucune entrée dans le journal disponible."

#: modules/dblog/dblog.admin.inc:97
msgid "Count"
msgstr "Nombre"

#: modules/dblog/dblog.admin.inc:140
msgid "Location"
msgstr "Emplacement"

#: modules/dblog/dblog.admin.inc:152;220
msgid "Severity"
msgstr "Importance"

#: modules/dblog/dblog.admin.inc:260
msgid "Filter log messages"
msgstr ""

#: modules/dblog/dblog.admin.inc:297
msgid "You must select something to filter by."
msgstr ""

#: modules/dblog/dblog.module:21
msgid ""
"The dblog module monitors your system, capturing system events in a log to "
"be reviewed by an authorized individual at a later time. This is useful for "
"site administrators who want a quick overview of activities on their site. "
"The logs also record the sequence of events, so it can be useful for "
"debugging site errors."
msgstr ""

#: modules/dblog/dblog.module:22
msgid ""
"The dblog log is simply a list of recorded events containing usage data, "
"performance data, errors, warnings and operational information. "
"Administrators should check the dblog report on a regular basis to ensure "
"their site is working properly."
msgstr ""

#: modules/dblog/dblog.module:23
msgid ""
"For more information, see the online handbook entry for <a href=\"@dblog"
"\">Dblog module</a>."
msgstr ""

#: modules/dblog/dblog.module:26
msgid ""
"The dblog module monitors your website, capturing system events in a log to "
"be reviewed by an authorized individual at a later time. The dblog log is "
"simply a list of recorded events containing usage data, performance data, "
"errors, warnings and operational information. It is vital to check the dblog "
"report on a regular basis as it is often the only way to tell what is going "
"on."
msgstr ""

#: modules/dblog/dblog.module:47
msgid ""
"Settings for logging to the Drupal database logs. This is the most common "
"method for small to medium sites on shared hosting. The logs are viewable "
"from the admin pages."
msgstr ""

#: modules/dblog/dblog.module:54
msgid "Recent log entries"
msgstr "Entrées récentes du journal"

#: modules/dblog/dblog.module:55
msgid "View events that have recently been logged."
msgstr "Voir les événements qui ont été enregistrés récemment."

#: modules/dblog/dblog.module:61
msgid "Top 'page not found' errors"
msgstr "Top des erreurs 'page non trouvée'"

#: modules/dblog/dblog.module:62
msgid "View 'page not found' errors (404s)."
msgstr "Voir les erreurs 'page non trouvée' (404)."

#: modules/dblog/dblog.module:68
msgid "Top 'access denied' errors"
msgstr "Top des erreurs 'accès refusé'"

#: modules/dblog/dblog.module:69
msgid "View 'access denied' errors (403s)."
msgstr "Voir les erreurs 'accès refusé' (403)."

#: modules/dblog/dblog.module:0
msgid "dblog"
msgstr ""

#: modules/dblog/dblog.install:25
msgid "Table that contains logs of all system events."
msgstr ""

#: modules/dblog/dblog.install:30
msgid "Primary Key: Unique watchdog event ID."
msgstr ""

#: modules/dblog/dblog.install:36
msgid "The {users}.uid of the user who triggered the event."
msgstr ""

#: modules/dblog/dblog.install:43
msgid "Type of log message, for example \"user\" or \"page not found.\""
msgstr ""

#: modules/dblog/dblog.install:49
msgid "Text of log message to be passed into the t() function."
msgstr ""

#: modules/dblog/dblog.install:55
msgid ""
"Serialized array of variables that match the message string and that is "
"passed into the t() function."
msgstr ""

#: modules/dblog/dblog.install:63
msgid "The severity level of the event; ranges from 0 (Emergency) to 7 (Debug)"
msgstr ""

#: modules/dblog/dblog.install:70
msgid "Link to view the result of the event."
msgstr ""

#: modules/dblog/dblog.install:75
msgid "URL of the origin of the event."
msgstr ""

#: modules/dblog/dblog.install:82
msgid "URL of referring page."
msgstr ""

#: modules/dblog/dblog.install:89
msgid "Hostname of the user who triggered the event."
msgstr ""

#: modules/dblog/dblog.install:95
msgid "Unix timestamp of when event occurred."
msgstr ""
